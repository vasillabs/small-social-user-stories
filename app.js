const express = require('express');
const path = require('path');
const ehphbs = require('express-handlebars');
const mongoose = require('mongoose');
const passport = require('passport');
const cookieParser = require('cookie-parser');
const session = require('express-session');

require('./models/User');
require('./config/passport')(passport);

const index = require('./routes/index');
const stories = require('./routes/stories');
const auth = require('./routes/auth');
const keys = require('./config/keys');

mongoose.connect(keys.mongoURI)
    .then(() => console.log('MongoDB Connected'))
    .catch(err => console.log(err));

const app = express();

app.engine('handlebars', ehphbs({
    defaultLayout: 'main'
}))
app.set('view engine', 'handlebars')

app.use(cookieParser());
app.use(session({
    secret: 'secret',
    resave: false,
    saveUninitialized: false
}))

app.use(passport.initialize())
app.use(passport.session())

app.use((req, res, next) => {
    res.locals.user = req.user || null;
    next();
})

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/stories', stories);

app.use('/auth', auth);

const port = process.env.PORT || 5000;

app.listen(port, function () {
    console.log(`Server started on port ${port}`);    
});